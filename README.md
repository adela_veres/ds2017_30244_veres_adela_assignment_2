## RPC application using distributed objects (with Java RMI) ##

### Setup ###

#### Pasi pentru incarcarea proiectului in eclipse
* Din meniul Eclipse se alege opțiunea File → Import.
* La deschiderea ferestrei Import se selectează Maven → Existing Maven Projects și apoi se selectează butonul Next.
* În fereastra Import Maven Projects, se completează câmpul Root Directory cu adresa locației proiectului în sistemul de fișiere propriu.
* După specificarea adresei, se vor selecta/bifa din panoul Projects, proiectul principal și toate submodulele acestuia.
* Apoi se va selecta opțiunea ‘Finish’.

#### Pasi pentru executia proiectului

* Pentru a putea pune în funcțiune programul este necesară crearea și pornirea unui registru de obiecte localizat remote , pentru portul specificat în cadrul aplicației. Acest lucru se realizează cu ajutorul comenzii rmiregistry. Astfel se va deschide un terminal/ command line și se va naviga în folderul rmi_server/target/classes, cu ajutorul comenzii cd, iar apoi se va apela comanda rmiregistry ce va rula în fundal.
* După încărcarea proiectului, se va selecta și se va rula ca și a aplicație Java, clasa ServerStart.java din modulul server, pachetul ro.tuc.dsrl.ds.handson.assig.two.server.communication. Se va afișa un mesaj corespunzător în consolă notificând astfel pornirea serverului.
* Se va rula în mod similar clasa ClientStart.java din modulul client, pachetul corespunzător fiind: ro.tuc.dsrl.ds.handson.assig.two.client.communication. 
* La pornirea clientului, se va deschide o fereastră de comunicare cu utilizatorul.
* Utilizatorul va introduce informațiile privitoare la propria mașină- anul fabricației, mărimea motorului, respectiv prețul de cumpărare al acesteia, în câmpurile corespunzătoare, iar apoi va selecta butonul ‘GET info’.
* La acționarea butonului se va afișa într-o zonă de text prețul posibil de vânzare a mașinii și taxa corespunzătoare, calculate conform algoritmilor specificați.
* Pentru aflarea unui nou preț/ a unei noi taxe, utilizatorul va putea modifica informațiile din câmpurile completate anterior, selectând din nou butonul ‘GET info’. Sistemul va reacționa în același fel precizat anterior, până la închiderea ferestrei, respectiv deconectarea clientului de la server.
