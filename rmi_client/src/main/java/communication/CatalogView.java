package communication;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class CatalogView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFabricationYear;
	private JTextField textEngineSize;
	private JTextField textPurchasingPrice;
	private JButton btnGetCarInfo;
	private JTextArea textArea;

	public CatalogView() {
		setTitle("Car information");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 200, 570, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertNewCar = new JLabel("Insert car information:");
		lblInsertNewCar.setBounds(50, 38, 180, 14);
		contentPane.add(lblInsertNewCar);

		JLabel lblFabricationYear = new JLabel("Fabrication year");
		lblFabricationYear.setBounds(50, 63, 100, 14);
		contentPane.add(lblFabricationYear);

		JLabel lblEngineSize= new JLabel("Engine size");
		lblEngineSize.setBounds(50, 88, 100, 14);
		contentPane.add(lblEngineSize);

		JLabel lblPurchasingPrice = new JLabel("Purchasing price");
		lblPurchasingPrice.setBounds(50, 113, 105, 14);
		contentPane.add(lblPurchasingPrice);

		textFabricationYear= new JTextField();
		textFabricationYear.setBounds(155, 60, 100, 20);
		contentPane.add(textFabricationYear);
		textFabricationYear.setColumns(10);

		textEngineSize = new JTextField();
		textEngineSize.setBounds(155, 85, 100, 20);
		contentPane.add(textEngineSize);
		textEngineSize.setColumns(10);

		textPurchasingPrice = new JTextField();
		textPurchasingPrice.setBounds(155, 110, 100, 20);
		contentPane.add(textPurchasingPrice);
		textPurchasingPrice.setColumns(10);

		btnGetCarInfo = new JButton("GET info");
		btnGetCarInfo.setBounds(60, 152, 89, 23);
		contentPane.add(btnGetCarInfo);

		textArea = new JTextArea();
		textArea.setBounds(275, 38, 190, 120);
		contentPane.add(textArea);
	
	}

	public void addBtnGetInfoActionListener(ActionListener e) {
		btnGetCarInfo.addActionListener(e);
	}

	public String getFabricationYear() {
		return textFabricationYear.getText();
	}

	public String getEngineSize() {
		return textEngineSize.getText();
	}

	public String getPurchasingPrice() {
		return textPurchasingPrice.getText();
	}

	public void printCarInfo(String carInfo) {
		textArea.setText(carInfo);
	}

	public void clear() {
		textFabricationYear.setText("");
		textEngineSize.setText("");
		textPurchasingPrice.setText("");
		textArea.setText("");
	}
}
