package communication;

import communication.Car;
import communication.ISellingPriceService;
import communication.ITaxService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;

public class CatalogController {
	private static final Log LOGGER = LogFactory.getLog(CatalogController.class);
	private static final String ERROR_MESSAGE = "An exception occured while trying connect to server. Please consult logs for more info!";

	private CatalogView catalogView;
	private ITaxService taxServiceInterface = null;
	private ISellingPriceService sellingPriceInterface = null;

	public CatalogController() {
		catalogView = new CatalogView();
		catalogView.setVisible(true);

		catalogView.addBtnGetInfoActionListener(new GetCarInfoActionListener());
	}

	public void displayErrorMessage(String message) {
		catalogView.clear();
		JOptionPane.showMessageDialog(catalogView, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Provides functionality for the 'GET info' button.
	 */
	class GetCarInfoActionListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			int carFabricationYear = Integer.parseInt(catalogView.getFabricationYear());
			int carEngineSize = Integer.parseInt(catalogView.getEngineSize());
			double carPurchasingPrice = Integer.parseInt(catalogView.getPurchasingPrice());
			Car car = new Car(carFabricationYear, carEngineSize, carPurchasingPrice);
			System.out.println(car.toString());
			
			try {
				sellingPriceInterface = (ISellingPriceService) Naming.lookup("rmi://localhost/SellingPrice");
				taxServiceInterface = (ITaxService) Naming.lookup("rmi://localhost/Tax");

				double sellingPrice = sellingPriceInterface.computeSellingPrice(car);
				double tax = taxServiceInterface.computeTax(car);

				String output = "Tax value: " + tax;
				output += "\nSelling price: " + sellingPrice;
				catalogView.printCarInfo(output);
				System.out.println(output);

			} catch (Exception ex) {
				displayErrorMessage(ERROR_MESSAGE);
				LOGGER.error("", ex);
				ex.printStackTrace();
			}
		}
	}

}
