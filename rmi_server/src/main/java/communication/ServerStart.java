package communication;

import java.rmi.*;

public class ServerStart {

	public static void main(String[] argv) {
		try {
			SellingPriceService sellingPriceService = new SellingPriceService();
			TaxService taxService = new TaxService();
			Naming.rebind("rmi://localhost/SellingPrice", sellingPriceService);
			Naming.rebind("rmi://localhost/Tax", taxService);
			
			System.out.println("SellingPrice/Tax Server is ready.");
		} catch (Exception e) {
			System.out.println("SellingPrice/Tax Server failed: " + e);
		}
	}
}
