package communication;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ISellingPriceService extends Remote{
	
	double computeSellingPrice(Car car) throws RemoteException;
}
