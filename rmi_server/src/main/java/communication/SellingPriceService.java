package communication;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class SellingPriceService extends UnicastRemoteObject implements ISellingPriceService{

	private static final long serialVersionUID = 1L;

	public SellingPriceService() throws RemoteException {
		super();
	}

	@Override
	public double computeSellingPrice(Car c) {
		double price = 0.0;
		double purchasingPrice = c.getPurchasingPrice();
		int year = c.getYear();
		price =  purchasingPrice - ( purchasingPrice/7.0*(2015 - year) ); 
		
		return price;
	}

}












