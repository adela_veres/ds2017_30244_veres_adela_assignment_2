package communication;

import java.io.Serializable;

public class Car implements Serializable {

	private static final long serialVersionUID = 1L;
	private int year;
	private int engineCapacity;
	private double purchasingPrice;

	public Car() {
	}

	public Car(int year, int engineCapacity) {
		this.year = year;
		this.engineCapacity = engineCapacity;
	}
	
	public Car(int year, int engineCapacity, double initPrice) {
		this.year = year;
		this.engineCapacity = engineCapacity;
		this.purchasingPrice = initPrice;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(int engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public double getPurchasingPrice() {
		return purchasingPrice;
	}
	
	public void setPurchasingPrice(double newPrice) {
		this.purchasingPrice = newPrice;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + engineCapacity;
		result = prime * result + year;
		result = (int) (prime * result + purchasingPrice);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (engineCapacity != other.engineCapacity)
			return false;
		if (year != other.year)
			return false;
		if (purchasingPrice != other.purchasingPrice)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Car [year=" + year + ", engineCapacity=" + engineCapacity + ", purchasingPrice=" + purchasingPrice + "]";
	}

}
